﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreKeeper : MonoBehaviour {

	public static int score = 0;
	//public float padding = 1f;
	private Text myText;
	
	//float xmin;
	//float xmax;
	//float ymax;
	
	void Start(){
		//float distance = transform.position.z - Camera.main.transform.position.z;
		//Vector3 leftmost = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distance));	
		//Vector3 rightmost = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, distance));
		//Vector3 topmost = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distance));
		//xmin = leftmost.x + padding;
		//xmax = rightmost.x - padding;
		//ymax = topmost.y - padding;
		transform.position = new Vector3(Screen.width - 250, Screen.height -75, 0f);
		myText = GetComponent<Text>();
		Reset ();
	}
	
	public void Score(int points){
		score += points;
		myText.text = "Score: " + score.ToString ();
	}

	public static void Reset(){
		score = 0;
	}
}
